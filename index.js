// console.log("Hello World!");

/*	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.


		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/

	function addTwoNum(additionResult) {
		let num1 = 5;
		let num2 = 15;
		additionResult = (num1 + num2)
		console.log("Displayed sum of " + num1 + " and " + num2 + ":")
		console.log(additionResult)
}
	
	addTwoNum();


		function subtractTwoNum(subtractionResult) {
			num1 = 20;
			num2 = 5;
			subtractionResult = (num1 - num2)
			console.log("Displayed difference of " + num1 + " and " + num2 + ":")
			console.log(subtractionResult)
	}
		
		subtractTwoNum();



/*
	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
*/
		function returnMultiTwoNum(result) {
			num1 = 50;
			num2 = 10;
			let multiResult = (num1 * num2)
			return multiResult;
			
		}

		let finalMultiResult = returnMultiTwoNum()
		console.log("The product of " + num1 + " and " + num2 + ":")
		console.log(finalMultiResult);


		function returnDiviTwoNum(result) {
			num1 = 50;
			num2 = 10;
			let diviResult = (num1 / num2)
			return diviResult;
			
		}

		let finalDiviResult = returnDiviTwoNum()
		console.log("The product of " + num1 + " and " + num2 + ":")
		console.log(finalDiviResult);


/*
	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
*/
		function returnAreaCircle(area) {
			let radius = 15;
			let pi = 3.1416;
			let areaCircleResult = (radius ** 2 * pi);
			return areaCircleResult;
		}

		let finalAreaCircleResult = returnAreaCircle();
		console.log("The result of getting the area of a circle with " + 15 + " radius:")
		console.log(finalAreaCircleResult)
				
/*
	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/	

		function returnAverage(average) {
			let n1 = 20, n2 = 40, n3 = 60, n4 = 80;
			let sum = n1 + n2 + n3 + n4;
			let n = 4
			let averageResult = (sum / n)
			return averageResult;
		}

		let finalReturnAverageResult = returnAverage();
		console.log("The average of 20,40,60 and 80:");
		console.log(finalReturnAverageResult)


/*
	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

		function passingScore(score) {
			num1 = 30
			num2 = 50
			let scorePercentage = num1 / num2 * 100;

			console.log("Is 38/50 a passing score?");
		}
		passingScore();
		
		
		let isPassingScore = true;
		console.log(isPassingScore);